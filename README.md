<p align="center"> <img alt="Markit" src="https://i.imgur.com/QjwPsaf.png" width="100%"/> </p>

Name
====

`Markit`, a Markdown parsing module for the [Raku](https://raku-lang.org) programming language.

Synopsis
========

    use Markit;
    my $md = Markdown.new;
    say $md.markdown("# Raku Rocks!"); # «<h1>Raku Rocks!</h1>␤»

Description
===========

This module parses Markdown (MD) and generates HTML from it.

How does Markit work?
---------------------

Similar to Parsedown, Markit tries to read a Markdown file like a human would. First, it looks at individual lines and check how the lines start in order to classify them into blocks of text (here a *block* is represented as a hash that contains information about the block of text. For instance, the block text, its type, the elements it contains, the routine it should be handled by, etc.). For example, if a line starts with a `*`, then it could be either a list's item (i.e., `li`) or a horizontal rule (i.e., `hr`). Once it recognizes the block, Markit continues to the line's content. As it reads, it watches out for special characters that helps it recognize inline elements (e.g., `*` for emphasis).

As stated in the Parsedown's repository, this is a "*line-based*" approach.

Installation
============

Using `zef`:

    zef update && zef install Markit

From source:

    git clone git@gitlab.com:uzluisf/raku-markit.git
    cd geuse && zef install .

Exports
=======

Class
-----

By default, `Markit` exports the `Markdown` class. The default instantiation

    my $md-obj = Markdown.new;

is the same as

    my $md-obj = Markdown.new:
        breaks-enabled =>  False,
        escape-markup  =>  False,
        urls-linked    =>  True,
        safe-mode      =>  False,
        strict-mode    =>  False
    ;

Or more succintly:

    my $md-obj = Markdown.new:
        :!breaks-enabled,
        :!escape-markup,
        :urls-linked,
        :!safe-mode,
        :!strict-mode
    ;

### Methods

The `Markdown` class makes available only the `markdown` method which must be called on an instance of the class. The following are the method's different signatures:

  * `markdown($text)` - Convert Markdown to HTML markup and return it.

        $md.markdown("# Raku Rocks!"); #=> <h1>Raku Rocks!</h1>

Authors
=======

  * Luis F. Uceta, [https://gitlab.com/uzluisf](https://gitlab.com/uzluisf)

License
=======

This module is a port of the PHP's Parsedown library written by Emanuil Rusev ([https://github.com/erusev](https://github.com/erusev)) and located at [https://github.com/erusev/parsedown](https://github.com/erusev/parsedown).

This program is free software; you can redistribute it and/or modify it under the Artistic License 2.0. See the LICENSE file included in this distribution for complete details.

