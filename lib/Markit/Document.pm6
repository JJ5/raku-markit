use Markit::Elements;
use Markit::Utility;

class Markit::Document {
    has Bool $.breaks-enabled = False;
    has Bool $.escape-markup  = False;
    has Bool $.urls-linked    = True;
    has Bool $.safe-mode      = False;
    has Bool $.strict-mode    = False;

    has %!definition-data;

=head1 Entry Point Method

    #| Return the corresponding HTML from the provided Markdown text.
    method text( Cool:D $md-text --> Str ) {
        my @elements = self.text-elements($md-text);
        my $markup   = self.elements(@elements).trim;
        return $markup;
    }

=head1 First Immediate Methods

    #| Return an array of text elements.
    method text-elements( Str $text is copy --> Array ) {
        # start definitions with a clean slate.
        %!definition-data = %();

        # standardize line breaks.
        $text = $text.subst(/<[\r\n \r]> $/, "\n", :g);

        # remove surrounding line breaks.
        $text = clip($text.&clip('\n'), '\n', :front);

        # split text into lines by newline.
        my @lines = $text.split(/\n/);

        # iterate through lines to identify blocks
        return self.lines-elements: Array[Str].new(@lines);
    }

    #| Return an array of identified blocks (Header, Paragraph, Code, etc.).
    method lines-elements( Str @lines, @non-nestables = [] --> Array ) {
        my Hash $current-block;
        my Hash @elements;      # collection of blocks represented as hashes

        OUTERMOSTFOR:
        for @lines <-> $l {
            my $line = $l;
            if clip($line) eq '' {
                with $current-block {
                    $_<interrupted> = $_<interrupted> ?? $_<interrupted> + 1 !! 1
                }
                next OUTERMOSTFOR;
            }

            # half-expand 1st tab occurrence in the line.
            $line ~~ s:1st/\t+/{' ' x $/.chars * 4 - $/.prematch.chars % 4}/;

            # create a hash representation of the line.
            my $indent = strspn($line, '\s');
            my $text   = $indent > 0 ?? $line.substr($indent, *) !! $line;
            my %line   = :body($line), :$indent, :$text;

            my Hash $block;

            if $current-block<continuable> {
                my Str $method-name = self.build-name(:block, $current-block, 'continue');

                $block = self.^find_method($method-name)
                    ?? self."$method-name"(%line, $current-block)
                    !! %();

                if $block {
                    $current-block = $block;
                    next OUTERMOSTFOR;
                }
                else {
                    if self.is-block-completable($current-block<type>) {
                        $method-name = self.build-name(:block, $current-block, 'complete');
                        $current-block = self.^find_method($method-name)
                            ?? self."$method-name"($current-block) !! %();
                    }
                }
            } # end test for continuable

            my Str $marker = %line<text>.substr(0, 1);

            # collect block types applicable to current line of text.
            my Str @block-types = |@Markit::Elements::unmarked-block-types;
            if %Markit::Elements::block-types{$marker}:exists {
                for %Markit::Elements::block-types{$marker}.flat -> $block-type {
                    @block-types.append($block-type);
                }
            }

            for @block-types -> $block-type {
                my Str $method-name = self.build-name(:block, $block-type);

                #$block = self."$method-name"(%line, $current-block);
                try   { $block = self."$method-name"(%line, $current-block) }
                if $! { note "Error: $method-name {$!.^name}" }

                if $block {
                    $block<type> = $block-type;
                    unless $block<identified> {
                        if $current-block {
                            @elements.append(self.extract-element($current-block));
                        }
                        $block<identified> = True;
                    }

                    if self.is-block-continuable($block-type) {
                        $block<continuable> = True;
                    }

                    $current-block = $block;

                    next OUTERMOSTFOR;
                }
            } # End of inner for loop

            if $current-block and $current-block<type> eq 'Paragraph' {
                $block = self.paragraph-continue(%line, $current-block);
            }

            if $block {
                 $current-block = $block
            }
            else {
                if $current-block {
                    @elements.append(self.extract-element($current-block));
                }
                $current-block = self.paragraph(%line);
                $current-block<identified> = True;
            }

        } # End OUTERMOSTFOR loop

        if $current-block<continuable> and self.is-block-completable($current-block<type>) {
            my Str $method-name = self.build-name(:block, $current-block, 'complete');
            $current-block  = self."$method-name"($current-block);
        }

        if $current-block {
            @elements.append(self.extract-element($current-block))
        }

        return @elements;
    }

    #| Stich together a collection of identified blocks into structured HTML.
    method elements( @elements --> Str ) {
        my Str $markup = '';
        my Bool $autobreak = True;

        for @elements.flat -> $element {
            next unless $element;

            my Bool $autobreak-next = $element<autobreak>
                ?? $element<autobreak> !! $element<name>.so;
            $autobreak = !$autobreak ?? $autobreak !! $autobreak-next;

            $markup ~= ($autobreak ?? "\n" !! '', self.element($element)).join;

            $autobreak = $autobreak-next;
        }

        $markup ~= $autobreak ?? "\n" !! '';
        return $markup;
    }

=head1 Second Immediate Methods

    #| Reassemble an identified block (i.e., an element) into a
    #| structured HTML string.
    method element( %element --> Str ) {
        %element = self.sanitize-element(%element) if $.safe-mode;
        %element = self.handle(%element);

        my Bool $has-name = %element<name>.so;
        my Str $markup    = '';

        # create first half (<) of opening tag and add attributes if any.
        if $has-name {
            $markup ~= ('<', %element<name>).join;
            if %element<attributes> {
                my @attributes = %element<attributes>.sort(*.key);
                for @attributes -> $pair {
                    my ($name, $value) = $pair.key, $pair.value;
                    next if $value === (Nil, Any).one;
                    $markup ~= (" $name", '="', escape($value), '"').join;
                }
            }
        }

        my Bool $permit-raw-html = False;
        my Str $text;
        if %element<text> {
            $text = %element<text>
        }
        elsif %element<raw-html> {
            $text = %element<raw-html>;
            $permit-raw-html = !$.safe-mode || %element<raw-html-in-safe-mode>;
        }

        my $has-content = $text || %element<element> || %element<elements>;
        if $has-content {
            # add second half (>) of opening tag.
            $markup ~= $has-name ?? '>' !! '';

            $markup ~= do
            if %element<elements>   { self.elements(%element<elements>) }
            elsif %element<element> { self.element(%element<element>) }
            else                    { !$permit-raw-html ?? escape($text, :allow-quotes) !! $text }

            # add complete closing tag.
            $markup ~= $has-name ?? "</{%element<name>}>" !! '';
        }
        elsif $has-name {
            $markup ~= '/>';
        }

        return $markup;
    }

    #| Return an array of elements assembled from a string.
    method line-elements( Str $text is copy, @nonnestables? --> Array ) {
        my @elements;
        my @non-nestables = @nonnestables.elems ?? @nonnestables.flat !! [] ;

        # standardize line breaks
        $text .= subst(/<[\r\n \r]>/, "\n", :g);

        OUTERWHILE:
        while my $excerpt  = search-for-characters($text, @Markit::Elements::inline-marker-list) {
            my Str  $marker     = $excerpt.substr(0, 1);
            my Int  $marker-pos = $text.chars - $excerpt.chars;
            my      %excerpt    = %(text => $excerpt, context => $text);
            my Str  $unmarked-text;
            my Hash $inline-text;

            for %Markit::Elements::inline-types{$marker}.flat -> $inline-type {
                next if @non-nestables».lc.contains($inline-type.lc);

                my Str $method-name = self.build-name(:inline, $inline-type);

                my $inline = try self."$method-name"(%excerpt);

                next unless $inline;

                next if $inline<position> and $inline<position> > $marker-pos;

                $inline<position> = $marker-pos without $inline<position>;

                $inline<element><non-nestables> = $inline<element><non-nestables>:exists
                    ?? ($inline<element><non-nestables>.flat, @non-nestables.flat)
                    !! @non-nestables.flat
                ;

                $unmarked-text = $text.substr(0, $inline<position>);

                $inline-text = self.inline-text($unmarked-text);
                @elements.append($inline-text<element>);

                @elements.append(self.extract-element($inline));

                $text = $text.substr($inline<position> + $inline<extent>, *);

                next OUTERWHILE;
            }

            $unmarked-text = $text.substr(0, $marker-pos + 1);
            $inline-text   = self.inline-text($unmarked-text);
            @elements.push($inline-text<element>);
            $text          = $text.substr($marker-pos + 1);
        } # End of while loop

        my Hash $inline-text = self.inline-text($text);
        @elements.push($inline-text<element>);

        for @elements <-> $element {
            $element<autobreak> = False unless $element<autobreak>
        }

        return @elements;
    } # line-elements

    #| Handle an element by updating the element's "destination" key through
    #| the function defined in the element's "handler" key if any.
    method handle( %element --> Hash ) {
        my ($function, $argument, $destination);
        if %element<handler> {
            if !%element<non-nestables> {
                %element<non-nestables> = [];
            }

            if %element<handler> ~~ Str {
                $function    = %element<handler>;
                $argument    = %element<text>;
                $destination = 'raw-html';
                %element<text>:delete;
            }
            elsif %element<handler> ~~ Hash {
                given %element<handler> -> $handler {
                    $function    = $handler<function>;
                    $argument    = $handler<argument>;
                    $destination = $handler<destination>;
                }
            }

            my $res = self."$function"($argument, %element<non-nestables>);

            %element{$destination} = $res;

            if $destination eq 'handler' {
                %element = self.handle(%element);
            }

            %element<handler>:delete;
        }

        return %element;
    } # handle

=head1 Blocks

=head2 Paragraph

    #| Return a hash representing a paragraph.
    method paragraph( %line --> Hash ) {
        my $block = %(
            type => 'Paragraph',
            element => %(
                name => 'p',
                handler => %(
                    function => 'line-elements',
                    argument => %line<text>,
                    destination => 'elements',
                ),
            ),
            procedence => &?ROUTINE.name,
        );

        return $block;
    } # paragraph

    method paragraph-continue( %line, Hash $current-block is copy --> Hash ) {
        return %() if $current-block<interrupted>;
        $current-block<element><handler><argument> ~= ("\n", %line<text>).join;
        return $current-block;
    } # paragraph-continue

=head2 Header

    #| Return a hash representing a '#'-marked header.
    method block-header( %line, *@ --> Hash ) {
        my $level = strspn(%line<text>, '#');

        return %() if $level > 6;

        my $text = %line<text>.&clip('#', :front).&clip('#');

        return %() if $.strict-mode and $text.substr(0, 1) and $text.substr(0, 1) ne ' ';

        $text .= trim;

        my $block = %(
            element => %(
                name => "h$level",
                handler => %(
                    function => 'line-elements',
                    argument => $text,
                    destination => 'elements',
                )
            ),
            procedence => &?ROUTINE.name,
        );

        return $block;
    } # block-header

=head2 Setext Header

    #| Return a hash representing a setext header.
    method block-setext-header( %line, $block is rw --> Hash ) {
        return %() if !$block or $block<type> ne 'Paragraph' or $block<interrupted>;

        if %line<indent> < 4 and clip(clip(%line<text>), %line<text>.substr(0, 1)) eq '' {
            $block<element><name> = %line<text>.substr(0, 1) eq '=' ?? 'h1' !! 'h2';
            return $block;
        }
        return %();
    } # block-setext-header

=head2 Code

    #| Return a hash representing a space-indented code block.
    method block-code( %line, Hash $block? --> Hash ) {
        if $block and $block<type> eq 'Paragraph' and !$block<interrupted> {
            return %()
        }

        if %line<indent> ≥ 4 {
            my $text = %line<body>.substr(4, *);
            my $block = %(
                element => %(
                    name    => 'pre',
                    element => %(
                        name => 'code',
                        text => $text,
                    ),
                ),
                procedence => &?ROUTINE.name,
            );

            return $block;
        }

        return %();
    } # block-code

    #| Handle a continuing space-indented block.
    method block-code-continue( %line, Hash $block --> Hash ) {
        if %line<indent> ≥ 4 {
            if $block<interrupted> {
                $block<element><element><text> ~= ("\n" x $block<interrupted>).join;
                $block<interrupted>:delete;
            }
            $block<element><element><text> ~= "\n";
            my $text = %line<body>.substr(4, *);
            $block<element><element><text> ~= $text;
            return $block;
        }

        return %();
    } # block-code-continue

    #| Handle a complete space-indented block.
    method block-code-complete( $block --> Hash ) {
        return $block;
    } # block-code-complete

=head2 Fenced Code

	#| Return a hash representing a fenced code block.
    method block-fenced-code( %line, *@ --> Hash) {
        my $marker = %line<text>.substr(0, 1);
        my $opener-length = strspn(%line<text>, $marker);

        return if $opener-length < 3;

        # for instance, language's name inside the code block
        my $infostring = %line<text>.substr($opener-length, *);

        # NOTE: This could be written more idiomatically. .contains?
        my $infostring-contains-opener = $infostring.index('`') ~~ Nil ?? False !! True;
        return if $infostring-contains-opener;

        my %element = %( name => 'code', text => '' );

        if $infostring ne '' {
            =begin comment
            https://www.w3.org/TR/2011/WD-html5-20110525/elements.html#classes
            Every HTML element may have a class attribute specified.
            The attribute, if specified, must have a value that is a set
            of space-separated tokens representing the various classes
            that the element belongs to.
            [...]
            The space characters, for the purposes of this specification,
            are U+0020 SPACE, U+0009 CHARACTER TABULATION (tab),
            U+000A LINE FEED (LF), U+000C FORM FEED (FF), and
            U+000D CARRIAGE RETURN (CR).
            =end comment
            my $lang = $infostring.substr(0, strcspn($infostring, '\s\t\n\f\r'));
            %element<attributes> = %(class => "language-$lang");
        }

        my %block = %(
            char => $marker,
            opener-length => $opener-length,
            element => %(
                name => 'pre',
                element => %element,
            ),
            procedence => &?ROUTINE.name,
        );

        return %block;
    } # block-fenced-code

	#| Handle a continuing fenced code block.
    method block-fenced-code-continue( %line, $block --> Hash ) {
        return if $block<complete>;

        if $block<interrupted> {
            $block<element><element><text> ~= "\n" x $block<interrupted>;
            $block<interrupted>:delete;
        }

        my Int $len = strspn(%line<text>, $block<char>);
        if $len >= $block<opener-length> &&
           %line<text>.substr($len, *).chop eq ''
        {
            $block<element><element><text> = $block<element><element><text>.substr(1, *);
            $block<complete> = True;
            return $block;
        }

        $block<element><element><text> ~= ("\n", %line<body>).join;

        return $block;
    } # block-fenced-code-continue

    #| Handle a complete fenced block.
    method block-fenced-code-complete( $block --> Hash ) {
        return $block;
    } # block-fenced-code-complete


=head2 List

    #| Return the hash representation for a list.
    method block-list( %line, $current-block? --> Hash ) {
        my ($name, $pattern) = do given %line<text>.substr(0, 1) {
            if $_ le '-' { ('ul', rx { <[*+-]> }                ) }
            else         { ('ol', rx { <[0..9]> ** 1..9 <[.)]> }) }
        }

        if %line<text>.match(/^ (<$pattern> $<spaces>=(\s+ | $)) $<list-item>=(.*) /) {
            my $match = $/;
            my $content-indent = $match[0]<spaces>.chars;

            my $marker-plus-space = $match[0].Str;
            my $list-item         = $match<list-item>.Str;

            if $content-indent ≥ 5 {
                $content-indent    -= 1;
                $marker-plus-space .= substr(0, * - $content-indent);
                $list-item          = ' ' x $content-indent ~ $list-item;
            }
            elsif $content-indent == 0 {
                $marker-plus-space ~= ' ';
            }

            my $spaceless-marker = $marker-plus-space.trim-trailing;

            my $block = %(
                indent  => %line<indent>,
                pattern => $pattern,
                data    => %(
                    type => $name,
                    marker => $marker-plus-space,
                    marker-type => ($name eq 'ul'
                        ?? $spaceless-marker
                        !! $spaceless-marker.substr(*-1)
                    ),
                ),
                element => %(
                    name     => $name,
                    elements => [],
                ),
                procedence => &?ROUTINE.name,
            );

            # NOTE: this is to escape special characters by placing
            # a backslash in front of them.
            $block<data><marker-type-regex> = '\\' ~
                $block<data><marker-type>.split('', :skip-empty).join('\\');

            if $name eq 'ol' {
                my $str = get-substring($/[0].Str, :before($block<data><marker-type>)).subst(/^0+/,'');
                my $list-start = $str ?? $str !! '0';

                if $list-start ne '1' {
                    return %() if $current-block
                       and $current-block<type> eq 'Paragraph'
                       and !$current-block<interrupted>;

                    $block<element><attributes> = %(start => $list-start);
                }
            }

            $block<li> = %(
                name => 'li',
                handler => %(
                    function => 'li',
                    argument => $match<list-item>.Str ?? [$match<list-item>.Str] !! [],
                    destination => 'elements'
                )
            );

            $block<element><elements>[0] := $block<li>;

            return $block;
        }

        return %();
    } # block-list


    #| Return the hash representation for a continuing list.
    method block-list-continue( %line, $block is copy --> Hash ) {
        return %() if $block<interrupted> and $block<li><handler><argument>.elems == 0;

        my Int $required-indent = $block<indent> + $block<data><marker>.chars;
        my $marker-type-regex   = $block<data><marker-type-regex>;

        my Bool $match-ordered = $block<data><type> eq 'ol' &&
            %line<text>.match(/^ <[0..9]>+ <$marker-type-regex> [\s+ (.*)|$] /).so;

        my Bool $match-unordered = $block<data><type> eq 'ul' &&
            %line<text>.match(/^ <$marker-type-regex> [\s+ (.*)|$] /).so;

        if %line<indent> < $required-indent and ($match-ordered or $match-unordered) {
            if $block<interrupted> {
                $block<li><handler><argument>.append('');
                $block<loose> = True;
                $block<interrupted>:delete;
            }

            $block<li>:delete;
            my $text       = $/[0].so ?? $/[0].Str !! '';
            $block<indent> = %line<indent>;

            $block<li> = %(
                name => 'li',
                handler => %(
                    function => 'li',
                    argument => [$text],
                    destination => 'elements'
                )
            );

            # get index to add next element to end of array
            my Int $next-end = $block<element><elements>.end ~~ Nil
                ?? 0 !! $block<element><elements>.end + 1;

            $block<element><elements>[$next-end] := $block<li>;

            return $block;
        }
        elsif %line<indent> < $required-indent and self.block-list(%line) {
            return %();
        }

        if %line<text>.substr(0, 1) eq '[' and self.block-reference(%line) {
            return $block;
        }

        if %line<indent> ≥ $required-indent {
            if $block<interrupted> {
                $block<li><handler><argument>.append('');
                $block<loose> = True;
                $block<interrupted>:delete;
            }

            my $text = %line<body>.substr($required-indent, *);
            $block<li><handler><argument>.append($text);

            return $block;
        }

        if !$block<interrupted> {
            my $text = %line<body>.subst(/^ \s ** {0..$required-indent} /, '');
            $block<li><handler><argument>.append($text);
            return $block;
        }

        return %();
    } # block-list-continue

    #| Return the hash representation for a complete list.
    method block-list-complete( $block --> Hash ) {
        if $block<loose> {
            for $block<element><elements>.flat <-> $li {
                if $li<handler><argument>.tail ne '' {
                    $li<handler><argument>.append('');
                }
            }
        }
        return $block;
    } # block-list-complete

    #| Return a hash representation of collection of lines of text considered
    #| to be a list's items.
    method li( @lines, *@ ) {
        my $elements = self.lines-elements: Array[Str].new(@lines);
        if  !@lines.grep('').so and $elements[0].defined and
            $elements[0]<name>  and $elements[0]<name> eq 'p'
        {
            $elements[0]<name>:delete;
        }

        return $elements;
    } # li


=head2 Table

    #| Return the hash representation for a table.
    method block-table( %line, $block is rw --> Hash ) {
        return %() if !$block or $block<type> ne 'Paragraph' or $block<interrupted>;

        if      !$block<element><handler><argument>.contains('|')
            and !%line<text>.contains('|')
            and !%line<text>.contains(':')
            or  $block<element><handler><argument>.contains("\n")
        {
            return %();
        }

        # TODO: Check into this
        #return %() if clip(%line<text>, '\s-:|') ne '';

        my @alignments    = [];
        my $divider       = %line<text>.trim;
           $divider      .= subst(/^ '|' | '|' $/, '', :global);
        my @divider-cells = $divider.split('|');

        for @divider-cells <-> $divider-cell {
            $divider-cell .= trim;

            return %() if $divider-cell eq '';

            my $alignment = '';

            # :----
            if $divider-cell.substr(0, 1) eq ':' {
                $alignment = 'left';
            }

            # ----: or :----:
            if $divider-cell.substr(*-1) eq ':' {
                $alignment = $alignment eq 'left' ?? 'center' !! 'right';
            }

            @alignments.push($alignment);
        }

        my @header-elements = [];
        my $header  = $block<element><handler><argument>;
           $header .= trim.subst(/^ '|' || '|' $/, '', :global);
        my @header-cells = $header.split('|', :skip-empty);

        return %() if @header-cells.elems != @alignments.elems;

        for @header-cells.kv -> $index, $hc {
            my $header-cell = $hc.trim;
            my %header-element = %(
                name => 'th',
                handler => %(
                    function => 'line-elements',
                    argument => $header-cell,
                    destination => 'elements',
                )
            );

            if @alignments[$index] {
                my $alignment = @alignments[$index];
                %header-element<attributes> = %(
                    style => "text-align: $alignment;",
                );
            }

            @header-elements.push(%header-element);
        }

        $block = %(
            alignments => @alignments,
            identified => True,
            element => %(
                name => 'table',
                elements => [],
            ),
            procedence => &?ROUTINE.name
        );

        $block<element><elements>.push(%(name => 'thead'));

        $block<element><elements>.push(%(name => 'tbody', elements => []));

        $block<element><elements>[0]<elements>.push(
            %( name => 'tr', elements => @header-elements,)
        );

        return $block;
    } # block-table

    #| Return the hash representation for a continuing table.
    method block-table-continue( %line, $block is rw --> Hash ) {
        return %() if $block<interrupted>;

        if $block<alignments>.elems == 1   or
           %line<text>.substr(0, 1) eq '|' or
           %line<text>.contains('|')
        {

            my @elements = [];
            my $row = %line<text>;
            $row = $row.trim.subst(/^ '|' || '|' $/, '', :global);

            my $match = $row.match(/[('\\|') | <-[|`]> | \` <-[`]>+ \` | \` ]+/, :global);

            # stringify the matches.
            my @items = $match».Str.list;

            my @cells = @items.splice(0, $block<alignments>.elems);

            for @cells.kv -> $index, $c {
                my $cell = $c.trim;
                my %element = %(
                    name => 'td',
                    handler => %(
                        function => 'line-elements',
                        argument => $cell,
                        destination => 'elements',
                    )
                );

                if $block<alignments>[$index] {
                    %element<attributes> = %(
                        style => "text-align: {$block<alignments>[$index]};",
                    );
                }

                @elements.push(%element);
            }

            my %element = %(
                name => 'tr',
                elements => @elements,
            );

            $block<element><elements>[1]<elements>.push(%element);
            $block<procedence> = &?ROUTINE.name;
            return $block
        }
    } # block-table-continue


=head1 Rule

    #| Return a hash representing a horizontal rule element.
    method block-rule( %line, *@ --> Hash ) {
        my $marker = %line<text>.substr(0, 1);
        my $block = %();

        if substr-count(%line<text>, $marker) ≥ 3 and
           clip(%line<text>, "\\s$marker") eq ''
        {
            $block<element> = %(name => 'hr');
        }
        return $block;
    }

=head2 Quote

    #| Return a hash representing a blockquote.
    method block-quote( %line, *@ --> Hash ) {
        my regex blockquote { ^ '>' \s* $<content>=(.*) }
        if %line<text>.match(/<blockquote>/) {
            my $block = %(
                element => %(
                    name => 'blockquote',
                    handler => %(
                        function => 'lines-elements',
                        argument => Array[Str].new($/<blockquote><content>.Str),
                        destination => 'elements',
                    )
                ),
                procedence => &?ROUTINE.name,
            );
            return $block;
        }

        return %();
    } # block-quote

    #| Return a hash representing a continuing blockquote.
    method block-quote-continue( %line, $block --> Hash ) {
        return %() if $block<interrupted>;

        my regex blockquote { ^ '>' \s* $<content>=(.*) }
        if %line<text>.substr(0, 1) eq '>' and %line<text>.match(/<blockquote>/) {
            $block<element><handler><argument>.push($/<blockquote><content>.Str);
            return $block;
        }

        unless $block<interrupted> {
            $block<element><handler><argument>.push(%line<text>);
            return $block;
        }

        return %();
    } # block-quote-continue

=head2 Markup

    #| Return a hash representing markup elements (br, div, etc.).
    method block-markup( %line, *@ --> Hash ) {
        return %() if $.escape-markup or $.safe-mode;

        my &tiny-rx = &Markit::Elements::html-attribute-regex;
        my regex markup {
            ^ \<
            '/'?
            (\w*)
            [ \s* <&tiny-rx> ]*
            '#'* ('/')?
            \>
        }

        if %line<text>.match(/<markup>/) {
            my Str $element = $/<markup>[0].lc;

            return %() if @Markit::Elements::text-level-elements.contains($element);

            my $block = %(
                name => $/<markup>[0].Str,
                element => %(
                    raw-html => %line<text>,
                    autobreak => True,
                ),
                procedence => &?ROUTINE.name
            );

            return $block;
        }

        return %();
    } # block-markup

    #| Return a hash representing continuing markup elements.
    method block-markup-continue( %line, $block --> Hash ) {
        return %() if $block<closed> or $block<interrupted>;
        $block<element><raw-html> ~= ("\n", %line<body>).join;
        return $block;
    } # block-markup-continue

=head2 Comment

    #| Return a hash representing HTML comments.
    method block-comment( %line, *@ --> Hash ) {
        return %() if $.escape-markup or $.safe-mode;

        return %() unless %line<text>.contains('<!--');

        if %line<text>.index('<!--') == 0 {
            my $block = %(
                element => %(
                    raw-html => %line<body>,
                    autobreak => True,
                ),
                procedence => &?ROUTINE.name
            );

            # close if this line already contains the comment's closing end.
            $block<closed> = True if %line<text>.contains('-->');

            return $block;
        }

        return %();
    } # block-comment

    #| Return a hash representing continuing HTML comments.
    method block-comment-continue( %line, $block --> Hash ) {
        return %() if $block<closed>;

        $block<element><raw-html> ~= ("\n", %line<body>).join;

        if %line<text>.contains('-->') {
            $block<closed> = True;
        }

        return $block;
    } # block-comment-continue

=head2 Reference

    #| Extract the ID information for the reference-style image syntax and
    #| makes it available through the %!definition-data attribute.
    method block-reference( %line, *@ --> Hash ) {
        my regex ref {
            ^ '[' $<id>=(.*?) ']'  # name of defined image reference
            ':'
            \s*
            '<'? $<url>=(\S+) '>'? # image's url
            [\s+
                <[" ' (]>
                $<title>=(.+)
                <[" ' )]>
            ]?                     # optional title attribute
            \s*
            $
        }

        if %line<text>.contains(']') and %line<text>.match(/<ref>/) {
            my Str $id = $/<ref><id>.lc;

            my %data = %(
                url   => $/<ref><url>.Str,
                title => $/<ref><title> ?? $/<ref><title>.Str !! Nil,
            );

            %!definition-data<reference>{$id} = %data;

            return %( element => %(), procedence => &?ROUTINE.name );
        }

        return %();
    } # block-reference

=head1 Inline Elements

=head2 Inline Text

    #| Return a hash representation of inline text.
    method inline-text( Str $text --> Hash ) {
        my Regex $rx = $.breaks-enabled
            ?? rx{ (\s* \n) }
            !! rx{ ([\s* '\\' | \s**2..*] \n) };

        my %inline = %(
            extent => $text.chars,
            element => %(
                elements => regex-replacement-elements(
                    $rx,
                    [%(name => 'br'), %(text => "\n")],
                    $text
                ),
            ),
            procedence => &?ROUTINE.name,
        );
        return %inline;
    }

=head2 Inline Code

    #| Return a hash representing an inline code element.
    method inline-code( %excerpt --> Hash ) {
        my Str $marker = %excerpt<text>.substr(0, 1);
        my $block = %();

        my $match = %excerpt<text>.match(
        /^ ($marker+) \s* $<content>=(.+?) \s* <!after $marker> $0 <!before $marker> /
        );

        if $match.so {
            my $text = $match<content>.Str;
            $text = $text.subst('/\s*\n/', ' ');

            my $block = %(
                extent => $match.chars,
                element => %(
                    name => 'code',
                    text => $text,
                ),
                procedence => &?ROUTINE.name,
            );

            return $block;
        }

        return %();
    } # inline-code

=head2 Inline Emphasis

    #| Return a hash representing inline text surrounded by characters
    #| (**, ****, etc.) denoting emphasis (either strong or em).
    method inline-emphasis( %excerpt --> Hash ) {
        return unless %excerpt<text>.substr(1,1);

        my $marker = %excerpt<text>.substr(0, 1);
        my Str $emphasis;

        my &strong = %Markit::Elements::strong-regex{$marker};
        my &emph   = %Markit::Elements::emphasis-regex{$marker};

        if %excerpt<text>.substr(1, 1) eq $marker and %excerpt<text>.match(&strong) {
            $emphasis = 'strong';
        }
        elsif $%excerpt<text>.match(&emph) {
            $emphasis = 'em';
        }
        else {
            return %();
        }

        my $block = %(
            extent => $/.chars,
            element => %(
                name => $emphasis,
                handler => %(
                    function     => 'line-elements',
                    argument     => $/<text>.Str,
                    destination  => 'elements',
                )
            ),
            procedence => &?ROUTINE.name,
        );

        return $block;
    } # inline-emphasis

=head2 Inline Strikethrough

    #| Return a hash representing a strikethrough element.
    method inline-strikethrough( %excerpt --> Hash ) {
        return unless %excerpt<text>.substr(1, 1);

        if %excerpt<text>.substr(1, 1) eq '~' and
           %excerpt<text>.match(/^ '~~' <?before \S> $<content>=(.*?) <?after \S> '~~'/)
        {
            my $block = %(
                extent => $/.chars,
                element => %(
                    name => 'del',
                    handler => %(
                        function => 'line-elements',
                        argument => $/<content>.Str,
                        destination => 'elements',
                    )
                ),
                procedence => &?ROUTINE.name,
            );
            return $block;
        }

        return %();
    } # inline-strikethrough

=head2 Inline Link

    #| Return a hash representing an inline link.
    method inline-link( %excerpt --> Hash ) {
        my $block = %();
        my %element = %(
            name => 'a',
            handler => %(
                function => 'line-elements',
                argument => Nil,
                destination => 'elements',
            ),
            'non-nestables' => ['Url', 'Link'],
            attributes => %(
                href => Nil,
                title => Nil,
            ),
        );

        my Int $extent = 0;
        my Str $remainder = %excerpt<text>;

        my regex r { \[ $<link-text>=( [ <-[ \] \[ ]>+ ]* ) \] }
        if $remainder.match(/\[ $<link-text>=( [ <-[ \] \[ ]>+ | <r> ]* ) \]/) {
            %element<handler><argument> = $/<link-text>.Str;
            $extent   += $/.chars;
            $remainder = $remainder.substr($extent, *);
        }
        else {
            return %();
        }

        my regex url-title { :ratchet
            ^
            '(' \s*
            $<url>=(
                [
                | <-[\s () ]>+?       # neither a whitespace nor parens
                | '(' <-[\s )]>+? ')' # parens around neither whitespace nor closing paren
                ]+
            )
            [ \s+
            $<title>=(
                | \" <-["]>*? \" # title with double quotes
                | \' <-[']>*? \' # title with single quotes
            )
            ]?
            \s* ')'
        }

        if $remainder.match(/<url-title>/) {
            %element<attributes><href> = $/<url-title><url>.Str;

            # ditch quotes and get link's title.
            if $/<url-title><title> {
                %element<attributes><title> = $/<url-title><title>.substr(1, *-1);
            }

            $extent += $/<url-title>.chars;
        }
        else {
            my $definition;
            if $remainder.match(/^ \s* \[ $<content>=(.*?) \]/) {
                $definition  = $/<content>.chars ?? $/<content> !! %element<handler><argument>;
                $definition .= lc;
                $extent     += $/.chars;
            }
            else {
                $definition = %element<handler><argument>.lc;
            }

            return %() if !%!definition-data<reference>{$definition};

            my %definition = %!definition-data<reference>{$definition};
            %element<attributes><href>  = %definition<url>;
            %element<attributes><title> = %definition<title>;
        }

        $block = %(
            extent => $extent,
            element => %element,
            procedence => &?ROUTINE.name,
        );

        return $block;
    } # inline-link

=head2 Inline Url

	#| Return a hash representing an inline url.
    method inline-url( %excerpt --> Hash ) {
        return %() if !$.urls-linked or !%excerpt<text>.substr(2, 1)
            or %excerpt<text>.substr(2, 1) ne '/';

        my regex url { :ignorecase
            <|w> https? \: '/'**2 <-[\s<]>+ <|w> '/'*
        }

        if %excerpt<context>.contains('http') and %excerpt<context>.match(/<url>/) {
            my Str $url = $/<url>.Str;

            my %inline = %(
                extent => $/<url>.chars,
                position => $/<url>.from,
                element => %(
                    name => 'a',
                    text => $url,
                    attributes => %(href => $url),
                ),
                procedence => 'inline-url',
            );

            return %inline;
        }

        return %();
    } # inline-url


=head2 Inline Url Tag

    #| Return a hash representing an inline url tag.
    method inline-url-tag( %excerpt ) {
        if %excerpt<text>.contains('>') and
        %excerpt<text>.match(/^ '<' $<url>=(\w+ ':' '/'**2 <-[\s >]>+) '>'/, :i) {
            my $url = $/<url>.Str;

            my $block = %(
                extent => $/.chars,
                element => %(
                    name => 'a',
                    text => $url,
                    attributes => %(href => $url),
                ),
                procedence => &?ROUTINE.name,
            );

            return $block;
        }
        return %();
    } # inline-url-tag

=head2 Inline Email Tag

    #| Return a hash representing an inline email tag.
    method inline-email-tag( %excerpt --> Hash ) {
        my regex hostname-label {
            <[a..z A..Z 0..9]> [<[a..z A..Z 0..9 -]>**0..61 <[a..z A..Z 0..9]>]?
        }

        my regex commonmark-email {
            <[a..z A..Z 0..9 . ! # $ % & ' * + / = ? ^ _ ` { | } ~ -]>+ '@'
            <hostname-label>
            ['.' <hostname-label> ]*
        }

        my regex whole-email { :ignorecase
            ^ '<' $<email>=($<mailto>=('mailto:')? <commonmark-email>) '>'
        }

        if %excerpt<text>.contains('>') and %excerpt<text>.match(/<whole-email>/) {
            my Str $url = $/<whole-email><email>.Str;
            $url = "mailto:$url" unless $/<whole-email><email><mailto>;

            my $block = %(
                extent => $/<whole-email>.chars,
                element => %(
                    name => 'a',
                    text => $/<whole-email><email>.Str,
                    attributes => %(href => $url),
                ),
                procedence => &?ROUTINE.name,
            );

            return $block;
        }

        return %();
    } # inline-email-tag


=head2 Inline Image

    #| Return a hash representing an inline image.
    method inline-image( %excerpt --> Hash ) {
        return %() if !%excerpt<text>.substr(1, 1)
                or %excerpt<text>.substr(1, 1) ne '[';

        # remove ! from the text's front.
        %excerpt<text> .= substr(1, *);
        my $link = self.inline-link(%excerpt);

        return %() unless $link;

        my %inline = %(
            extent => $link<extent> + 1,
            element => %(
                name => 'img',
                attributes => %(
                    src => $link<element><attributes><href>,
                    alt => $link<element><handler><argument>,
                ),
                autobreak => True,
            ),
            procedence => &?ROUTINE.name,
        );

        %inline<element><attributes>.push($link<element><attributes>.flat);

        %inline<element><attributes><href>:delete;

        return %inline;
    } # inline-image

=head2 Inline Markup

    #| Return a hash representing inline markup. See block-markup and
    #| block-continuing-markup for block-level markup elements.
    method inline-markup( %excerpt --> Hash ) {
        return %() if $.escape-markup or $.safe-mode or !%excerpt<text>.contains('>');

        my $block = %(
            procedence => &?ROUTINE.name,
        );

        if %excerpt<text>.substr(1, 1) eq '/' and
        %excerpt<text>.match(/^ '</' \w <[\w -]>* \s* '>' /) {
            $block<element> = %(raw-html => $/.Str);
            $block<extent>  = $/.chars;
            return $block;
        }

        if %excerpt<text>.substr(1, 1) eq '!' and
        %excerpt<text>.match(/^ '<!--' '-'? <-[>-]> ['-'? <-[-]>]* '-->'/) {
            $block<element> = %(raw-html => $/.Str);
            $block<extent>  = $/.chars;
            return $block;
        }

        my &rx = &Markit::Elements::html-attribute-regex;
        if %excerpt<text>.substr(1, 1) ne ' ' and
        %excerpt<text>.match(/^ '<' \w <[\w -]>* [\s* <&rx>]* \s* '/'? '>'/) {
            $block<element> = %(raw-html => $/.Str);
            $block<extent>  = $/.chars;
            return $block;
        }

        return %();
    } # inline-markup

=head2 Inline Escape Sequence

    #| Return a hash representing an inline escapable character.
    method inline-escape-sequence( %excerpt --> Hash ) {
        my $char = %excerpt<text>.substr(1, 1);

        if $char and @Markit::Elements::special-characters.contains($char) {
            my $block = %(
                element => %(raw-html => $char),
                extent => 2,
                procedence => 'inline-escape-sequence',
            );

            return $block;
        }

        return %();
    } # inline-escape-sequence


=head2 Inline Special Characters

	#| Return a hash representing an inline special character.
    method inline-special-character( %excerpt --> Hash ) {
        my regex entity {
           ^ '&' ('#'? <[0..9 a..z A..Z]>+) ';'
        }

        if %excerpt<text>.substr(1, 1) ne ' ' and
           %excerpt<text>.contains(';')       and
           %excerpt<text>.match(/<entity>/)
        {
            return %(
                element => %(raw-html => ('&', $/<entity>[0].Str, ';').join),
                extent => $/<entity>.chars,
                procedence => &?ROUTINE.name,
            );
        }

        return %();
    } # inline-special-character

=head1 Helper Methods

    #| Return a string (a method's name) from the block's type
    #| and the action supplied.
    multi method build-name( $blck, Str $action, Bool :$block! --> Str ) {
        ('block', $blck<type>.lc, $action).join('-')
    }

    #| Return a string (a method's name) from the block's type.
    multi method build-name( $block-type, Bool :$block! --> Str ) {
        ('block', $block-type.lc).join('-')
    }

    #| Return a string (a method's name) from the inline element's type.
    multi method build-name( $inline-type, Bool :$inline! --> Str ) {
        ('inline', $inline-type.lc).join('-');
    }

    #| Check if block is continuable by verifying that there exists
    #| a method (of type 'continue') that handles its type.
    method is-block-continuable( Str $type --> Bool ) {
        my $method = ('block', $type.lc, 'continue').join('-');
        self.^find_method($method).so
    }

    #| Check if block is completable by verifying that there exists
    #| a method (of type 'complete') that handles its type.
    method is-block-completable( Str $type --> Bool ) {
        my $method = ('block', $type.lc, 'complete').join('-');
        self.^find_method($method).so
    }

    #| Set a component's "element" key unless it's been set already.
    method extract-element( Hash $component --> Hash ) {
        unless $component<element> {
            if $component<markup> {
                $component<element> = raw-html => $component<markup>
            }
            elsif $component<hidden> {
                $component<element> = %()
            }
        }
        return $component<element>
    } # extract-element

    method sanitize-element( %element --> Hash ) {

        my regex good-attribute {
            ^
            <[a..zA..Z0..9]> <[a..zA..Z0..9 _ -]>*
            $
        }

        my %safe-url2attr = %(
            a   => 'href',
            img => 'src',
        );

        unless %element<name> {
            %element<attributes>:delete;
            return %element;
        }

        if %safe-url2attr{ %element<name> } {
            %element = self.filter-unsafe-url-attribute(
                %element,
                %safe-url2attr{%element<name>}
            );
        }

        if %element<attributes> {
            for %element<attributes>.kv -> $attr, $val {
                # filter out badly parsed attribute
                if !$attr.match(/<good-attribute>/) {
                    %element<attributes>{$attr}:delete;
                }
                # dump onevent attribute
                elsif $attr.starts-with('on') {
                    %element<attributes>{$attr}:delete;
                }
            }
        }

        return %element;
    } # sanitize-element

    method filter-unsafe-url-attribute( %element, $attribute --> Hash ) {

        for @Markit::Elements::safe-links-whitelist -> $scheme {
            if %element<attributes>{$attribute}.starts-with($scheme) {
                return %element
            }
        }

        %element<attributes>{$attribute} .= subst(':', '%3A');

        return %element;
    } # filter-unsafe-url-attribute
}
