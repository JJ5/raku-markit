unit module Markit::Elements;

our %block-types = %(
    '#' => <Header>,
    '*' => <Rule List>,
    '+' => <List>,
    '-' => <Setext-Header Table Rule List>,
    '0' => <List>,
    '1' => <List>,
    '2' => <List>,
    '3' => <List>,
    '4' => <List>,
    '5' => <List>,
    '6' => <List>,
    '7' => <List>,
    '8' => <List>,
    '9' => <List>,
    ':' => <Table>,
    '<' => <Comment Markup>,
    '=' => <Setext-Header>,
    '>' => <Quote>,
    '[' => <Reference>,
    '_' => <Rule>,
    '`' => <Fenced-Code>,
    '|' => <Table>,
    '~' => <Fenced-Code>,
);

our @unmarked-block-types = <Code>;

our @inline-marker-list = <! * & [ : \< ` ~ _ \\>;

our %inline-types = %(
    '!'  => <Image>,
    '&'  => <Special-Character>,
    '*'  => <Emphasis>,
    ':'  => <Url>,
    '<'  => <Url-Tag Email-Tag Markup>,
    '['  => <Link>,
    '_'  => <Emphasis>,
    '`'  => <Code>,
    '~'  => <Strikethrough>,
    '\\' => <Escape-Sequence>,
);

our @special-characters = < \ ` *  _ { } [ ] () \> # + - . ! | ~ >;

our @void-elements = <area base br col command embed hr img input link meta param source>;

our @text-level-elements = (
    'a', 'br', 'bdo', 'abbr', 'blink', 'nextid', 'acronym', 'basefont',
    'b', 'em', 'big', 'cite', 'small', 'spacer', 'listing',
    'i', 'rp', 'del', 'code',          'strike', 'marquee',
    'q', 'rt', 'ins', 'font',          'strong',
    's', 'tt', 'kbd', 'mark',
    'u', 'xm', 'sub', 'nobr',
               'sup', 'ruby',
               'var', 'span',
               'wbr', 'time',
);

our @safe-links-whitelist = (
    'http://',
    'https://',
    'ftp://',
    'ftps://',
    'mailto:',
    'tel:',
    'data:image/png;base64,',
    'data:image/gif;base64,',
    'data:image/jpeg;base64,',
    'irc:',
    'ircs:',
    'git:',
    'ssh:',
    'news:',
    'steam:',
);

# NOTE: Although tests are passing, these regexes might need some tweaking.
our %emphasis-regex = %(
    # for *TEXT*
    '*' => rx {
        ^
        \*
        $<text>=(
            [
                | '\\*'
                | <-[*]>
                | '**' <-[*]>*? '**'
            ]+?
        )
        \*
        <!before '*'>
    },

    # for _TEXT_
    '_' => rx {
        ^
        '_'
        $<text>=(
            [
               | '\\_'              # a single _
               | <-[_]>             # any character other than _
               | '__' <-[_]>* '__'  # any character (except _) padded with __
            ]+?                     # match frugally
        )
        '_'
        <!before '_'>
        <|w>
    }
);

our %strong-regex = %(
    # for **TEXT**
    '*' => rx {
        ^
        '*' ** 2                   # Match '*' exactly twice
        $<text>=(
            [
                | '\\*'            # match this tring literally
                | <-[*]>           # any character other than *
                | '*' <-[*]>*? '*' # any character other than * padded with *
            ]+?                    # match group frugally
        )
        '*' ** 2                   # Match '*' exactly twice
        <!before '*'>              # Match if not immediately followed by a *
    },

    # for __TEXT__
    '_' => rx {
        ^
        '_' ** 2                   # Match '_' exactly twice
        $<text>=(
            [
                | '\\_'
                | <-[_]>
                | '_' <-[_]>*? '_'
            ]+?
        )
        '_' ** 2                   # Match '_' exactly twice
        <!before '_'>              # Match if not immediately followed by a *
    }
);

our regex html-attribute-regex {
    <[ a..z A..Z _ :]>
    <[ \w : . - ]>*
    [
        \s* '=' \s*              # spaces around the = sign
        [
        | <-[ " ' = < > ` \s ]>+ # anything but these characters
        | '"' <-[ " ]>* '"'      # double quotes around non-double quotes
        | "'" <-[ ' ]>* "'"      # single quotes around non-single quotes
        ]
    ]
    ?
}

