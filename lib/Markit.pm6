use Markit::Document;
unit module Markit;

class Markdown is export(:DEFAULT) {
    has Bool $.breaks-enabled is rw = False;
    has Bool $.escape-markup  is rw = False;
    has Bool $.urls-linked    is rw = True;
    has Bool $.safe-mode      is rw = False;
    has Bool $.strict-mode    is rw = False;

    multi method markdown( Cool:D $text --> Str ) {
        my $markup = self!get-document.text($text);
        return $markup;
    }

    method !get-document {
        Markit::Document.new:
            :$!breaks-enabled,
            :$!escape-markup,
            :$!urls-linked,
            :$!safe-mode,
            :$!strict-mode
        ;
    }
}

sub markdown(
    Cool:D $text,
    Bool :$breaks-enabled = False,
    Bool :$escape-markup  = False,
    Bool :$urls-linked    = True,
    Bool :$safe-mode      = False,
    Bool :$strict-mode    = False,
    --> Str
) is export(:sub) {
    Markdown.new(:$breaks-enabled, :$escape-markup, :$urls-linked,
        :$safe-mode, :$strict-mode
    ).markdown($text)
}

=begin pod

<p align="center">
<img alt="Markit" src="https://i.imgur.com/QjwPsaf.png" width="100%"/>
</p>

=head1 Name

C«Markit», a Markdown parsing module for the L«Raku|https://raku-lang.org»
programming language.

=head1 Synopsis
=begin code
use Markit;
my $md = Markdown.new;
say $md.markdown("# Raku Rocks!"); # «<h1>Raku Rocks!</h1>␤»
=end code

=head1 Description

This module parses Markdown (MD) and generates HTML from it.

=head2 How does Markit work?

Similar to Parsedown, Markit tries to read a Markdown file like a human would.
First, it looks at individual lines and check how the lines start in order
to classify them into blocks of text (here a I«block» is represented as a hash
that contains information about the block of text. For instance, the block
text, its type, the elements it contains, the routine it should be handled by,
etc.). For example, if a line starts with a C«*», then it could be either a list's item
(i.e., C«li») or a horizontal rule (i.e., C«hr»). Once it recognizes the block,
Markit continues to the line's content. As it reads, it watches out for special
characters that helps it recognize inline elements (e.g., C«*» for emphasis).

As stated in the Parsedown's repository, this is a "I«line-based»" approach.

=head1 Installation

Using C«zef»:

=for code
zef update && zef install Markit

From source:

=for code
git clone git@gitlab.com:uzluisf/raku-markit.git
cd geuse && zef install .

=head1 Exports

=head2 Class

By default, C«Markit» exports the C«Markdown» class. The default instantiation

=begin code
my $md-obj = Markdown.new;
=end code

is the same as

=begin code
my $md-obj = Markdown.new:
    breaks-enabled =>  False,
    escape-markup  =>  False,
    urls-linked    =>  True,
    safe-mode      =>  False,
    strict-mode    =>  False
;
=end code

Or more succintly:

=begin code
my $md-obj = Markdown.new:
    :!breaks-enabled,
    :!escape-markup,
    :urls-linked,
    :!safe-mode,
    :!strict-mode
;
=end code

=head3 Methods

The C«Markdown» class makes available only the C«markdown» method
which must be called on an instance of the class. The following are the
method's different signatures:

=begin item
C«markdown($text)» - Convert Markdown to HTML markup and return it.

=begin code
$md.markdown("# Raku Rocks!"); #=> <h1>Raku Rocks!</h1>
=end code
=end item

=head2 Subroutines

You can export the subroutine C«markdown» which is just a wrapper
around the C«Markdown.markdown» method. Export with the C«:sub» tag:

=begin code
use Markit :sub;
markdown("# Raku Rocks!"); # «<h1>Raku Rocks!</h1>␤»
=end code

=head1 Authors

=item Luis F. Uceta, L«https://gitlab.com/uzluisf»

=head1 License

This module is a port of the PHP's Parsedown library written by Emanuil Rusev
(L«https://github.com/erusev») and located at L«https://github.com/erusev/parsedown».

This program is free software; you can redistribute it and/or modify it under
the Artistic License 2.0. See the LICENSE file included in this distribution for
complete details.

=end pod
